# 南橋 API 文件
## MQTT API Library 說明資料

### 裝置連線管理及V&V（verification and validation， 簡稱V&V）:支援以下感測元件透過此 MQTT API Library 將資料上傳至資料中心
|元件|Data format|Protocol|
|:--|:--|:--|
|GNSS|JSON|MQTT
|ECU|JSON|MQTT
|IMU|JSON|MQTT
|BMS|JSON|MQTT
|LiDAR|JSON|MQTT
|RADAR|JSON|MQTT


### V&V 檢查結果

##### 正常結果
```
{
    "source": {
        "vid": "7856bc123456", 
        "gnss": [
            {
                "coord": [
                    24.1784264631, 
                    121.647543
                ], 
                "heading": 250.12345678, 
                "source_time": 1552295943651, 
                "speed": 100.999, 
                "timestamp": 1552295943651
            }
        ]
    }, 
    "message": "OK", 
    "status": 200
}
```

##### 錯誤內容
```
{
  "source": {},
  "message": "Field 'vid' not found.Field 'gnss' not found.",
  "status": 400
}
```
|status|說明|
|:--|:--|
|200|正常結果
|400|錯誤內容


### 下載範例程式及 API Library
|項目|說明|下載連結
|:--|:--|:--|
|南僑 API 連線範例程式|提供 MQTT 裝置連線 API 及使用範例程式，讓開發者能透過範例來加速開發，減少所需之時程；此為Java 版本。|* Java 版本 [Click](src/test/java/com/wistron/occ/south/bound/OpenMqttClientTest.java)<br>* API Library [Click](src/main/resources/occ-mqtt-api-0.1.jar)

### 組態設定
##### pom.xml

```
   <!-- occ-mqtt-api -->
		<dependency>
			<groupId>occ.mqtt</groupId>
			<artifactId>occ-mqtt-api</artifactId>
			<version>0.1</version>
		</dependency>
```

##### 執行畫面
![測試](img/img.png)