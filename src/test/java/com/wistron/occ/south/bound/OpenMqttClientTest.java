package com.wistron.occ.south.bound;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wistron.occ.iot.service.common.LogUtil;
import com.wistron.occ.iot.service.mqtt.Listener;
import com.wistron.occ.iot.service.mqtt.OpenMqttClient;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class OpenMqttClientTest {
	
	String host = "ssl://10.34.172.109:8883"; // change to your project MQTT broker host
	// ca 、 cert、key
	String[] authenticate = { // change to your project authenticate file path
			"ca path",
			"cert path",
			"key path" };

	String subTopic[] = { "roadside/signalcontrol/motcspat" }; // change to your project subscriber topic
	String pubTopic = "vehicle/report/wst"; // change to your project publish topic

	@Test
	public void test() throws MqttException, InterruptedException {

		OpenMqttClient omc = new OpenMqttClient(host,authenticate, subTopic); // subscriber message

		omc.setListener(new Listener() {

			@Override
			public void connectionLost(Throwable cause) {
				log.error(LogUtil.concat("Connection lost!"));
				log.error(LogUtil.concat("msg " + cause.getMessage()));
				log.error(LogUtil.concat("loc " + cause.getLocalizedMessage()));
				log.error(LogUtil.concat("cause " + cause.getCause()));
				log.error(LogUtil.concat("excep " + cause));
				omc.startReconnect();
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
				try {
					MqttDeliveryToken token = (MqttDeliveryToken) iMqttDeliveryToken;
					String str = token.getMessage().toString();
					log.info(LogUtil.concat("deliverd message :", str));
				} catch (MqttException me) {
					log.error(LogUtil.concat("reason " + me.getReasonCode()));
					log.error(LogUtil.concat("msg " + me.getMessage()));
					log.error(LogUtil.concat("loc " + me.getLocalizedMessage()));
					log.error(LogUtil.concat("cause " + me.getCause()));
					log.error(LogUtil.concat("excep " + me));
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			@Override
			public void messageArrived(String topic, MqttMessage msg) throws Exception {
				log.info(LogUtil.concat("message Arrived"));
				log.info(LogUtil.concat("topic of name:" + topic));
				log.info(LogUtil.concat("message of Qos:" + msg.getQos()));
				log.info(LogUtil.concat("message of content:" + new String(msg.getPayload())));
			}
		});

		omc.start(); // wait for incoming message

		for (;;) {
			Thread.sleep(5000L);
			System.out.println(omc.server.publish(omc.server.GNSS, pubTopic, "{}")); // change the rawdata
		}
	}
}



